# ImageMachine
---  
## Features  

- Sort the list of machines by name or type.  
- Add machine data
- Update machine data
- Delete machine data.  
- Scan QR Code Machine.  

--- 
## Usage  


Clone this git and open the file that has the extension `.xcworkspace`

There are 2 main views 

**Home** (List of Machine)  

![alt text](https://imgur.com/download/uRbqZHR "Logo Title Text 1")
![alt text](https://imgur.com/download/iZERwjV "Logo Title Text 1")   
   
In this view, the user will get a list of the machines that have been inputted before.  
    
The following 4 interactions can be done by the user: 

1. Sort the list of machines by name or type of machine by pressing the button found in the upper left part of navigation.  
2. Delete machine data by swiping left on the machine data list.  
3. Open the screen to add machine data by pressing the yellow (*plus*) button at the bottom center of the display.  
4. Open QR Code scanner by pressing the button found in the upper right part of navigation.

**Detail Machine** (Add / Update)

![alt text](https://imgur.com/download/xxGQVQY "Logo Title Text 1")
![alt text](https://imgur.com/download/mbJEcRL "Logo Title Text 1")  

In this view, the user will get machine data details that contain information on *Machine ID*, *Machine Name*, *Machine Type*, *QR Code Number*, and date of *Last Maintenance*.  

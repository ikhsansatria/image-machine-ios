//
//  CustomAnimatedTextInputStyle.swift
//  Image Machine
//
//  Created by Ikhsan Satria on 29/11/18.
//  Copyright © 2018 Ikhsan Satria. All rights reserved.
//

import UIKit

struct CustomTextInputStyle: AnimatedTextInputStyle {
    var textInputFont: UIFont = AppConstant.Font.CircularBook12
    let activeColor = UIColor.white
    let inactiveColor = AppConstant.Color.Primary
    let errorColor = UIColor.red
    let textInputFontColor = UIColor.white
    let placeholderMinFontSize: CGFloat = 11
    let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 9)
    let leftMargin: CGFloat = 0
    let topMargin: CGFloat = 20
    let rightMargin: CGFloat = 0
    let bottomMargin: CGFloat = 10
    let yHintPositionOffset: CGFloat = 7
    let yPlaceholderPositionOffset: CGFloat = 10
    let lineInactiveColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
}

//
//  QRCodeReaderVC.swift
//  Image Machine
//
//  Created by Ikhsan Satria on 29/11/18.
//  Copyright © 2018 Ikhsan Satria. All rights reserved.
//

import UIKit
import AVFoundation

protocol QRCodeReaderVCDelegate {
    
    func didRetrive(value: String)
}

class QRCodeReaderVC: AppVC {
    
    @IBOutlet weak var cameraView: UIView!
    
    var delegate: QRCodeReaderVCDelegate?
    
    var session: AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCode = String()
    
    let pointerSize: CGFloat = 20
    let selectionBorderWidth: CGFloat = 1
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        session?.startRunning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        session?.stopRunning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Code Reader"
        
        // Set capture device
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        
        var error: NSError? = nil    //Possible Error to be thrown
        let captureDeviceInput: AnyObject?
        
        // Tries to get device input, shows alert if theres an error
        do {
            captureDeviceInput = try AVCaptureDeviceInput(device: captureDevice) as AVCaptureDeviceInput
        } catch let someError as NSError{
            error = someError   //get Error to throw
            captureDeviceInput = nil
        }
        
        // Shows Error if failure to get Input
        if error != nil{
            alertPromptToAllowCameraAccessViaSetting()
            return
        }
        
        // Creates capture session and adds input
        session = AVCaptureSession()
        session?.addInput(captureDeviceInput as! AVCaptureInput)
        
        // Get metadata output from session
        let metadataOutput = AVCaptureMetadataOutput()
        session?.addOutput(metadataOutput)
        
        metadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_queue_main_t.main)
        metadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        // Add the video preview layer to the main view
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session!)
        cameraView.layoutIfNeeded()
        
        videoPreviewLayer?.frame = self.cameraView.bounds
        
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        self.cameraView.layer.addSublayer(videoPreviewLayer!)
        
        // Run session
        session?.startRunning()
    }
}

// MARK: - AVCaptureMetadataOutputObjectsDelegate
extension QRCodeReaderVC: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count < 1 {
            //self.resetToDefault()
            return
        }
        
        // Get each metadataObject and check if it is a barcode type
        let metadataMachineReadableCodeObject = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataMachineReadableCodeObject.type == AVMetadataObject.ObjectType.qr {
            
            // let barcode = self.videoPreviewLayer?.transformedMetadataObject(for: metadataMachineReadableCodeObject as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
            
            if let qrCodeValue = metadataMachineReadableCodeObject.stringValue {
                if delegate != nil {
                    delegate!.didRetrive(value: qrCodeValue)
                }
            } else {
                showSnackbar(message: "QRCode not recognised", backgroundColor: AppConstant.Color.Thirdly)
            }
        }
    }
}

// MARK: - CALayerDelegate
extension QRCodeReaderVC: CALayerDelegate {
    func layoutSublayers(of layer: CALayer) {
        if layer == self.videoPreviewLayer {
            self.videoPreviewLayer?.frame = self.cameraView.bounds
        }
    }
}

extension QRCodeReaderVC {
    /// Show alert prompt to allow camera access via setting.
    func alertPromptToAllowCameraAccessViaSetting() {
        let laterAction = UIAlertAction(title: "Later", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        let showSettingAction = UIAlertAction(title: "Settings", style: .cancel) { (action) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        }
        
        showAlert(
            title: "Please Allow Access",
            message: "Image Machine needs access to your camera for Scaning QR Code. \n\n Please open Settings > Privacy > Camera and set Image Machine to On",
            actions: [laterAction, showSettingAction]
        )
    }
}


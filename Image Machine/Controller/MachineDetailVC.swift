//
//  MachineDetailVC.swift
//  Image Machine
//
//  Created by Ikhsan Satria on 29/11/18.
//  Copyright © 2018 Ikhsan Satria. All rights reserved.
//

import UIKit
import CoreData
import SimpleImageViewer
import ActionSheetPicker_3_0
import DKImagePickerController
import MaterialComponents.MaterialButtons

protocol MachineDetailDataDelegate: AnyObject {
    func updateMachine(_ machine: Machine)
    
    func addMachine(uuid: UUID, name: String, type: String, qrCode: Int, updatedOn: NSDate, images: [MachineImage])
}

class MachineDetailVC: AppVC, UIGestureRecognizerDelegate {

    @IBOutlet weak var uuidInput: AnimatedTextInput!
    @IBOutlet weak var nameInput: AnimatedTextInput!
    @IBOutlet weak var typeInput: AnimatedTextInput!
    @IBOutlet weak var qrCodeInput: AnimatedTextInput!
    @IBOutlet weak var lastMaintenanceInput: AnimatedTextInput!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addImageButton: MDCFloatingButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    private var updateOn: Date? {
        didSet {
            lastMaintenanceInput.text = (updateOn != nil) ? updateOn!.toString(withFormat: "dd MMMM yyyy") : ""
        }
    }
    
    weak var delegate: MachineDetailDataDelegate?
    var machine: Machine?

    var machineImages: [MachineImage] = [] {
        didSet {
            if machineImages.count >= 10 {
                isShowAddImageButton = false
            }
        }
    }
    
    var indexImageForRemove: [IndexPath] = []

    var isDeletionImageCollection: Bool = false {
        didSet {
            if oldValue != isDeletionImageCollection {
                switch isDeletionImageCollection {
                case true:
                    navigationItem.rightBarButtonItem?.title = "Delete"
                    navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.8078431373, green: 0.2470588235, blue: 0.2666666667, alpha: 1)
                    
                    /// Set right bar button item.
                    let cancelBarButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(cancelImageEditingMode))
                    navigationItem.setCustomLeftBarButtonItem(cancelBarButton, tintColor: #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1))
                    
                    isShowAddImageButton = false
                    
                    break
                case false:
                    if navigationItem.rightBarButtonItem?.title == "Delete" {
                        navigationItem.rightBarButtonItem?.title = "Save"
                        navigationItem.rightBarButtonItem?.tintColor = AppConstant.Color.Primary
                        
                        leftButtonImage = #imageLiteral(resourceName: "BackIcon")
                        
                        isShowAddImageButton = (machineImages.count < 10)
                    }
                    break
                }
                self.collectionView.reloadData()
            }
        }
    }
    
    var isTimerRunning = false,
        seconds = 1,
        timer = Timer()
    
    var isShowAddImageButton: Bool {
        get { return false }
        set {
            switch newValue {
            case true:
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
                    self.addImageButton.alpha = 1
                }, completion: nil)
                break
            case false:
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
                    self.addImageButton.alpha = 0
                }, completion: nil)
                break
            }
        }
    }

    ///
    fileprivate func configFormInput() {
        setAnimatedTextInputConfig(uuidInput, placeholder: "UUID")
        uuidInput.isEnabled = false
        setAnimatedTextInputConfig(nameInput, placeholder: "Name")
        setAnimatedTextInputConfig(typeInput, placeholder: "Type")
        setAnimatedTextInputConfig(qrCodeInput, placeholder: "QR Code Number", type: .numeric)
        setAnimatedTextInputConfig(lastMaintenanceInput, placeholder: "Last Maintenance", type: .selection, tapAction: showDatePickerView)
    }
    
    /// Show date picker view for input *Last Maintenance*
    fileprivate func showDatePickerView () {
        view.endEditing(true)
        
        let datePicker = ActionSheetDatePicker(title: "Last Maintenance", datePickerMode: .date, selectedDate: updateOn ?? Date(), doneBlock: {
            picker, value, index in
            if let choosedDate = value as? Date {
                self.updateOn = choosedDate
            }
            return
        }, cancel: { ActionSheetDate in return }, origin: lastMaintenanceInput)
        
        datePicker?.show()
    }
    
    fileprivate func setFormInputData(machine: Machine) {
        uuidInput.text = machine.uuid.uuidString
        nameInput.text = machine.name
        typeInput.text = machine.type
        qrCodeInput.text = "\(machine.qrcode)"
        updateOn = machine.updatedOn as Date
        machineImages = machine.images != nil ? machine.images!.allObjects as! [MachineImage] : []
        machineImages = machineImages.sorted(by: { $0.createdAt.isLessThan(date: $1.createdAt) })
        self.collectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configFormInput()
        
        if machine != nil {
            title = "Edit Machine"
            setFormInputData(machine: machine!)
        } else {
            title = "Add Machine"
            uuidInput.text = UUID().uuidString
        }
        
        /// Set right bar button item.
        let rightBarButton = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(rightBarButtonAction(_:)))
        navigationItem.setCustomRightBarButtonItem(rightBarButton)
        
        let tapToDismiss = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(_:)))
        self.view.addGestureRecognizer(tapToDismiss)
        
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(cellLongPressHandler(_:)))
        lpgr.minimumPressDuration = 0.1
        lpgr.delegate = self
        lpgr.delaysTouchesBegan = true
        self.collectionView.addGestureRecognizer(lpgr)
        
        let buttonScheme = MDCButtonScheme()
        // Config Button of Add Data Image.
        MDCFloatingActionButtonThemer.applyScheme(buttonScheme, to: addImageButton)
        addImageButton.setBackgroundColor(AppConstant.Color.Secondary)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updateCollectionViewHeight()
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.collectionView.alpha = 1.0
        }) { (completion) in
            self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        }
    }
    
    func updateCollectionViewHeight() {
        let collectionViewHeight = collectionView.collectionViewLayout.collectionViewContentSize.height
        collectionViewHeightConstraint.constant = collectionViewHeight
        self.view.setNeedsLayout()
        self.view.setNeedsDisplay()
    }
    
    @objc private func rightBarButtonAction(_ sender: UIBarButtonItem) {
        switch sender.title == "Save" {
        case true:
            validateFormInput {
                guard let delegation = self.delegate else { return }

                switch self.machine != nil {
                case true:
                    
                    self.machine!.name = self.nameInput.text!
                    self.machine!.type = self.typeInput.text!
                    self.machine!.qrcode = Int32(self.qrCodeInput.text!)!
                    self.machine!.updatedOn = self.updateOn != nil ? self.updateOn! as NSDate : NSDate()
                    self.machine!.images = NSSet(array: self.machineImages)

                    delegation.updateMachine(self.machine!)
                    break
                case false:
                    delegation.addMachine(
                        uuid: UUID(uuidString: self.uuidInput.text!)!,
                        name: self.nameInput.text!,
                        type: self.typeInput.text!,
                        qrCode: Int(self.qrCodeInput.text!)!,
                        updatedOn: (self.updateOn != nil) ? self.updateOn! as NSDate : NSDate(),
                        images: self.machineImages
                    )
                    break
                }
            }
            break
        case false:
            if (indexImageForRemove.count > 0) {
                let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                actionSheet.addAction(UIAlertAction(title: "Delete", style: .destructive) { (action) in
                    self.collectionView.performBatchUpdates({
                        print(self.indexImageForRemove)
                        
                        self.indexImageForRemove.forEach({ (item) in
                            self.machineImages.remove(at: item.row)
                        })
                        print(self.machineImages.count)
                        self.collectionView.deleteItems(at: self.indexImageForRemove)
                        self.indexImageForRemove = []
                    }, completion: { (completion) in
                        self.isDeletionImageCollection = false
                    })
                })
    
                actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
                present(actionSheet, animated: true, completion: nil)
            }
            break
        }
    }
    
    @objc func cellLongPressHandler(_ gesture : UILongPressGestureRecognizer!) {
        switch gesture.state {
        case .began:
            runTimer()
            break
        case .ended:
            timer.invalidate()
            seconds = 1
            break
        default:
            break
        }
    }

    ///
    @objc fileprivate func updateTimer() {
        seconds -= 1
        if seconds <= 0 && isDeletionImageCollection == false {
            isDeletionImageCollection = true
        }
    }

    ///
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @objc func cancelImageEditingMode() {
        isDeletionImageCollection = false
    }
    
    @IBAction func addDataImage(_ sender: UIButton) {
        
        let pickerController = DKImagePickerController()
        pickerController.sourceType = .photo
        pickerController.assetType = .allPhotos
        pickerController.maxSelectableCount = 10 - machineImages.count
        pickerController.allowsLandscape = false
        pickerController.showsCancelButton = true
        
        pickerController.didSelectAssets = { (assets: [DKAsset]) in

            let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            
            assets.forEach({ (asset) in
                let managedContext = appDelegate.persistentContainer.viewContext
                
                let machineImage = MachineImage(context: managedContext)

                asset.fetchImage(with: layout.itemSize.toPixel(), completeBlock: { (image, info) in
                    guard let dataImage = image?.jpegData(compressionQuality: 100) else { return }
                    
                    machineImage.uuid = UUID()
                    machineImage.data = dataImage as NSData
                    machineImage.createdAt = NSDate()
                    
                    self.machineImages.append(machineImage)
                })
            })

            self.collectionView.reloadData()
            self.updateCollectionViewHeight()

        }
        
        self.present(pickerController, animated: true, completion: nil)
    }
    
    @objc func showFullScreenImage(_ sender: UITapGestureRecognizer) {
        
        guard let rowIndex = sender.view?.tag else { return }
        
        self.collectionView.delegate?.collectionView!(collectionView, didSelectItemAt: IndexPath(row: rowIndex, section: 0))
    }
}

// MARK: - UICollectionViewDataSource
extension MachineDetailVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return machineImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MachineImageCollectionVCell
        cell.delegate = self
        
        let machineImage = machineImages[indexPath.row]
        cell.machineImageView.image = UIImage(data: machineImage.data as Data)
        cell.machineImageView.contentMode = .scaleAspectFill
        cell.machineImageView.layer.masksToBounds = true
        cell.clipsToBounds = true
        cell.tag = indexPath.row
        cell.machineImageView.isUserInteractionEnabled = true
        cell.machineImageView.tag = indexPath.row
        
        let fullScreenTagGesture = UITapGestureRecognizer(target: self, action: #selector(showFullScreenImage(_:)))
        cell.machineImageView.addGestureRecognizer(fullScreenTagGesture)
        
        cell.isDeletion = self.isDeletionImageCollection

        return cell
    }
}

// MARK: - MachineImageCollectionViewCellDelegate
extension MachineDetailVC: MachineImageCollectionViewCellDelegate {
    func removeImageFor(_ row: Int, isSelected: Bool) {
        switch isSelected {
        case true:
            indexImageForRemove.append(IndexPath(row: row, section: 0))
            break
        case false:
            indexImageForRemove.removeAll(where: { $0.row == row })
            break
        }
        indexImageForRemove.sort(by: { $0.row > $1.row })
    }
}

// MARK: - UICollectionViewDelegate
extension MachineDetailVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let configuration = ImageViewerConfiguration { config in
            let machineImage = machineImages[indexPath.row]
            config.image = UIImage(data: machineImage.data as Data)?.withRenderingMode(.alwaysOriginal)
        }

        present(ImageViewerController(configuration: configuration), animated: true)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension MachineDetailVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let devider: CGFloat = AppUtility.screenWidth <= 375 ? 3 : 4
        let width = AppUtility.screenWidth / devider - (devider - 1)
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
}

// MARK: - AnimatedTextInputDelegate
extension MachineDetailVC: AnimatedTextInputDelegate {
    func animatedTextInput(animatedTextInput: AnimatedTextInput, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        switch animatedTextInput {
        case qrCodeInput:
            return (animatedTextInput.text?.isNumber(string: string))!
        default:
            return true
        }
    }
}

extension MachineDetailVC {
    /// Validate every element of form from any input mistake.
    ///
    /// - Parameter completion: Void that can be run after all validation is successful.
    fileprivate func validateFormInput(completion: (() -> Void)? = nil) {
        if (nameInput.text?.trimmingCharacters(in: .whitespaces).count)! < 1 {
            _ = nameInput.becomeFirstResponder()
        } else if (typeInput.text?.trimmingCharacters(in: .whitespaces).count)! < 1 {
            _ = typeInput.becomeFirstResponder()
        }  else if (qrCodeInput.text?.trimmingCharacters(in: .whitespaces).count)! < 1 {
            _ = qrCodeInput.becomeFirstResponder()
        } else if updateOn == nil {
            showDatePickerView()
        } else {
            if let complete = completion {
                DispatchQueue.main.async() { complete() }
            }
        }
    }
}

//
//  MachineVC.swift
//  Image Machine
//
//  Created by Ikhsan Satria on 29/11/18.
//  Copyright © 2018 Ikhsan Satria. All rights reserved.
//

import UIKit
import CoreData
import MaterialComponents.MaterialButtons

class MachineVC: AppVC {
    
    var machines: [Machine] = [],
        indexRowSelectedMachine: Int!
    
    @IBOutlet weak var addButton: MDCFloatingButton!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillLayoutSubviews() {
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Machine Data"
        
        let footerTableView = UIView()
        footerTableView.backgroundColor = AppConstant.Color.Thirdly
        tableView.tableFooterView = footerTableView
        
        leftButtonImage = #imageLiteral(resourceName: "SortIcon")
        rightBarButtonImage = #imageLiteral(resourceName: "QRCodeIcon")
        
        let buttonScheme = MDCButtonScheme()
        
        // Config Button of Add Data.
        MDCFloatingActionButtonThemer.applyScheme(buttonScheme, to: addButton)
        addButton.setBackgroundColor(AppConstant.Color.Secondary)
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext =  appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<Machine>(entityName: "Machine")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        
        do {
            machines = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    override func leftBarButtonItemClicked() {
        let actionSheet = UIAlertController(title: "Sort by:", message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Name", style: .default) { (action) in
            self.machines.sort(by: {$0.name < $1.name})
            self.tableView.reloadData()
        })
        
        actionSheet.addAction(UIAlertAction(title: "Type", style: .default) { (action) in
            self.machines.sort(by: {$0.type < $1.type})
            self.tableView.reloadData()
        })
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    override func rightBarButtonItemClicked() {
        let qrCodeReaderVC = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeReaderVC") as! QRCodeReaderVC
        qrCodeReaderVC.delegate = self
        
        let navController = UINavigationController(rootViewController: qrCodeReaderVC)
        navController.navigationBar.isTranslucent = false
        
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func addData(_ sender: UIButton) {
        let machineDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "MachineDetailVC") as! MachineDetailVC
        machineDetailVC.delegate = self
        
        let navController = UINavigationController(rootViewController: machineDetailVC)
        self.present(navController, animated: true, completion: nil)
    }
    
    private func removeData(for indexPath: IndexPath) {
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        managedContext.delete(machines[indexPath.row])
        
        do {
            try managedContext.save()
            machines.remove(at: indexPath.row)
        } catch {
            print(error)
        }
    }
}

// MARK: - UITableViewDataSource
extension MachineVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return machines.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let machine = machines[indexPath.row]
        
        cell.textLabel?.text = machine.name
        cell.detailTextLabel?.text = machine.type
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension MachineVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let machineDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "MachineDetailVC") as! MachineDetailVC
        indexRowSelectedMachine = indexPath.row

        machineDetailVC.delegate = self
        machineDetailVC.machine = machines[indexPath.row]
        self.navigationController?.pushViewController(machineDetailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            self.removeData(for: indexPath)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}

// MARK: - MachineDetailDataDelegate
extension MachineVC: MachineDetailDataDelegate {
    func updateMachine(_ machine: Machine) {
        do {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            let managedContext = appDelegate.persistentContainer.viewContext
            
            machines[indexRowSelectedMachine] = machine

            try managedContext.save()
            
            self.tableView.reloadData()
            self.navigationController?.popViewController(animated: true)
        } catch let error as NSError {
            print(error.localizedDescription)
            showSnackbar(message: "Could not to update machine data", backgroundColor: .orange, messageAction: nil)
        }
    }
    
    func addMachine(uuid: UUID, name: String, type: String, qrCode: Int, updatedOn: NSDate, images: [MachineImage]) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Machine", in: managedContext)!
        let machine = Machine(entity: entity, insertInto: managedContext)
        
        machine.uuid = uuid
        machine.name = name
        machine.type = type
        machine.qrcode = Int32(qrCode)
        machine.updatedOn = updatedOn
        machine.images = NSSet(array: images)

        do {
            try managedContext.save()
            self.dismiss(animated: true) {
                self.machines.append(machine)
                self.tableView.reloadData()
            }
        } catch let error as NSError {
            print(error.localizedDescription)
            showSnackbar(message: "Could not to add machine data", backgroundColor: .orange, messageAction: nil)
        }
    }
}

// MARK: - QRCodeReaderVCDelegate
extension MachineVC: QRCodeReaderVCDelegate {
    func didRetrive(value: String) {
        if let indexMachine = machines.firstIndex(where: { $0.qrcode == Int32(value) }) {
            self.dismiss(animated: true) {
                let machineDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "MachineDetailVC") as! MachineDetailVC
                self.indexRowSelectedMachine = indexMachine
                machineDetailVC.delegate = self
                machineDetailVC.machine = self.machines[indexMachine]
                self.navigationController?.pushViewController(machineDetailVC, animated: true)
            }
        } else {
            showSnackbar(message: "Machine not found", backgroundColor: AppConstant.Color.Thirdly)
        }
    }
}

//
//  AppVC.swift
//  Image Machine
//
//  Created by Ikhsan Satria on 29/11/18.
//  Copyright © 2018 Ikhsan Satria. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialSnackbar

class AppVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        leftButtonImage = #imageLiteral(resourceName: "BackIcon")
        
        setDefaultHeader()
    }
    
    func setDefaultHeader(_ barTintColor: UIColor? = UIColor.white) {
        self.navigationController?.navigationBar.titleTextAttributes = [
            .font: AppConstant.Font.CircularBold16,
            .foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        ]
        self.navigationController?.navigationBar.barTintColor = AppConstant.Color.Thirdly
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    ///
    var leftButtonImage: UIImage? {
        get { return nil }
        set {
            let barButtonItem = UIBarButtonItem(image: newValue, style: .done, target: self, action: #selector(leftBarButtonItemClicked))
            barButtonItem.tintColor = AppConstant.Color.Primary
            navigationItem.setLeftBarButton(barButtonItem, animated: true)
        }
    }
    
    ///
    @objc func leftBarButtonItemClicked() {
        if navigationController?.popViewController(animated: true) == nil {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    ///
    var rightBarButtonImage: UIImage? {
        get { return nil }
        set {
            let barButtonItem = UIBarButtonItem(image: newValue, style: .done, target: self, action: #selector(rightBarButtonItemClicked))
            barButtonItem.tintColor = AppConstant.Color.Primary
            navigationItem.rightBarButtonItem = barButtonItem
        }
    }
    
    ///
    @objc func rightBarButtonItemClicked() { }
    
    /// Set configuration for **AnimatedTextInput**.
    func setAnimatedTextInputConfig(_ textInput: AnimatedTextInput, placeholder: String, type: AnimatedTextInput.AnimatedTextInputType? = nil, tapAction: (() -> Void)? = nil) {
        textInput.style = CustomTextInputStyle()
        textInput.placeHolderText = placeholder
        textInput.backgroundColor = .clear
        if type != nil { textInput.type = type! }
        if let action = tapAction { textInput.tapAction = action }
        textInput.delegate = self as? AnimatedTextInputDelegate
    }
    
    ///
    ///
    /// - Parameters:
    ///   - message:
    ///   - backgroundColor:
    ///   - messageAction:
    func showSnackbar(message: String, backgroundColor: UIColor = AppConstant.Color.Primary, messageAction: MDCSnackbarMessageAction? = nil) {
        if MDCSnackbarManager.hasMessagesShowingOrQueued() == false {
            let snackbarMessage = MDCSnackbarMessage()
            snackbarMessage.text = message
            
            if messageAction != nil {
                snackbarMessage.action = messageAction!
            }
            
            MDCSnackbarManager.snackbarMessageViewBackgroundColor = backgroundColor
            MDCSnackbarManager.messageFont = AppConstant.Font.CircularBook13
            MDCSnackbarManager.buttonFont = AppConstant.Font.CircularBold14
            MDCSnackbarManager.setButtonTitleColor(.black, for: .normal)
            MDCSnackbarManager.show(snackbarMessage)
        }
    }
    
    func showAlert(title: String, message: String, actions: [UIAlertAction]? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        if let alertActions = actions {
            alertActions.forEach { (action) in
                alert.addAction(action)
            }
        } else {
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(defaultAction)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

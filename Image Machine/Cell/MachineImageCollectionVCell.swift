//
//  MachineImageCollectionVCell.swift
//  Image Machine
//
//  Created by Ikhsan Satria on 29/11/18.
//  Copyright © 2018 Ikhsan Satria. All rights reserved.
//

import UIKit

protocol MachineImageCollectionViewCellDelegate {
    func removeImageFor(_ row: Int, isSelected: Bool)
}

class MachineImageCollectionVCell: UICollectionViewCell {
    
    @IBOutlet weak var machineImageView: UIImageView!
    @IBOutlet weak var checkmarkButton: UIButton!
    
    var isDeletion: Bool = false {
        didSet {
            if oldValue != isDeletion  {
                checkmarkButton.isHidden = !isDeletion
                checkmarkButton.isSelected = false
            }
        }
    }
    
    var deletionStatus: Bool = false
    
    var delegate: MachineImageCollectionViewCellDelegate?
    
    @IBAction func checkmarkAction(_ sender: UIButton) {
        if isDeletion == true {
            sender.isSelected = (true && !sender.isSelected)
            if delegate != nil {
                delegate!.removeImageFor(self.tag, isSelected: sender.isSelected)
            }
        }
    }
}

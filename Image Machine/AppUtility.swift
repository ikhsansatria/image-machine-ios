//
//  AppUtility.swift
//  Image Machine
//
//  Created by Ikhsan Satria on 01/12/18.
//  Copyright © 2018 Ikhsan Satria. All rights reserved.
//

import UIKit

struct AppUtility {    
    static let screenWidth = UIApplication.shared.statusBarOrientation == .portrait || UIApplication.shared.statusBarOrientation == .portraitUpsideDown ? UIScreen.main.bounds.size.width : UIScreen.main.bounds.size.height
    
    static let screenHeight = UIApplication.shared.statusBarOrientation == .portrait || UIApplication.shared.statusBarOrientation == .portraitUpsideDown ? UIScreen.main.bounds.size.height : UIScreen.main.bounds.size.width
}

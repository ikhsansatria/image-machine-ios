//
//  AppConstant.swift
//  Image Machine
//
//  Created by Ikhsan Satria on 29/11/18.
//  Copyright © 2018 Ikhsan Satria. All rights reserved.
//

import UIKit

struct AppConstant {
    enum Font {
        typealias RawValue = UIFont
        static let CircularBook12 = UIFont(name: "CircularStd-Book", size: 12)!
        static let CircularBook13 = UIFont(name: "CircularStd-Book", size: 13)!
        static let CircularBold12 = UIFont(name: "CircularStd-Bold", size: 12)!
        static let CircularBold14 = UIFont(name: "CircularStd-Bold", size: 14)!
        static let CircularBold16 = UIFont(name: "CircularStd-Bold", size: 16)!
    }
    let Font: Font
    
    enum Color {
        typealias RawValue = UIFont
        static let Primary = #colorLiteral(red: 0.1979215741, green: 0.7626095414, blue: 0.5741896629, alpha: 1)
        static let Secondary = #colorLiteral(red: 0.9882352941, green: 0.8117647059, blue: 0.2666666667, alpha: 1)
        static let Thirdly = #colorLiteral(red: 0.2588235294, green: 0.262745098, blue: 0.3019607843, alpha: 1)
    }
    let Color: Color
}

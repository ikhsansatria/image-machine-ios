//
//  Machine+CoreDataProperties.swift
//  Image Machine
//
//  Created by Ikhsan Satria on 29/11/18.
//  Copyright © 2018 Ikhsan Satria. All rights reserved.
//
//

import Foundation
import CoreData


extension Machine {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Machine> {
        return NSFetchRequest<Machine>(entityName: "Machine")
    }

    @NSManaged public var uuid: UUID
    @NSManaged public var name: String
    @NSManaged public var type: String
    @NSManaged public var qrcode: Int32
    @NSManaged public var updatedOn: NSDate
    @NSManaged public var images: NSSet?

}

// MARK: Generated accessors for images
extension Machine {

    @objc(addImagesObject:)
    @NSManaged public func addToImages(_ value: MachineImage)

    @objc(removeImagesObject:)
    @NSManaged public func removeFromImages(_ value: MachineImage)

    @objc(addImages:)
    @NSManaged public func addToImages(_ values: NSSet)

    @objc(removeImages:)
    @NSManaged public func removeFromImages(_ values: NSSet)

}

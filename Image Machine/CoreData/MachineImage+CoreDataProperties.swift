//
//  MachineImage+CoreDataProperties.swift
//  Image Machine
//
//  Created by Ikhsan Satria on 30/11/18.
//  Copyright © 2018 Ikhsan Satria. All rights reserved.
//
//

import Foundation
import CoreData


extension MachineImage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MachineImage> {
        return NSFetchRequest<MachineImage>(entityName: "MachineImage")
    }

    @NSManaged public var data: NSData
    @NSManaged public var uuid: UUID
    @NSManaged public var createdAt: NSDate
    @NSManaged public var machine: NSSet?

}

// MARK: Generated accessors for machine
extension MachineImage {

    @objc(addMachineObject:)
    @NSManaged public func addToMachine(_ value: Machine)

    @objc(removeMachineObject:)
    @NSManaged public func removeFromMachine(_ value: Machine)

    @objc(addMachine:)
    @NSManaged public func addToMachine(_ values: NSSet)

    @objc(removeMachine:)
    @NSManaged public func removeFromMachine(_ values: NSSet)

}
